# safe\_cell

This crate exports the `SafeCell` type, a wrapper type that enables safe exterior mutability for arbitrary contents.

The use case is similar to that of the standard library's `UnsafeCell`[¹][unsafecell] type but, by virtue of being specialized for situations where it can be statically proven that no unsound access occurs, `SafeCell` is fully usable in safe code. In addition, the implementation is easily proven to be fully sound, making `SafeCell` a great alternative to `UnsafeCell` in safety-critical code.

As the implementation is incredibly lightweight and does not make use of any additional synchronization primitives or dynamic borrow tracking, it has negligible overhead (and hence functions as a true "zero-cost abstraction"[²][zerocost]).

[unsafecell]: https://doc.rust-lang.org/std/cell/struct.UnsafeCell.html
[zerocost]: https://boats.gitlab.io/blog/post/zero-cost-abstractions/
